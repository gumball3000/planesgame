package sample;

import java.io.Serializable;

public class Hit implements Serializable {
    private int[] hit;
    private int[][] plane;
    private String message;
    private static final long serialVersionUID = 1L;

    public Hit(int[] hit, String message) {
        this.hit = hit;
        this.message = message;
    }

    public Hit(String message) {
        this.message = message;
    }

    public Hit(int[] hit, int[][] plane, String message) {
        this.hit = hit;
        this.plane = plane;
        this.message = message;
    }

    public int[] getHit() {
        return hit;
    }

    public String getMessage() {
        return message;
    }

    public int[][] getPlane() {
        return plane;
    }


}
