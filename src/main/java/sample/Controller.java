package sample;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import javax.xml.soap.Text;
import java.net.Socket;


public class Controller {


    @FXML
    private Button server;

    @FXML
    private Button client;

    @FXML
    private Button connect;

    @FXML
    private Button sendHello;

    @FXML
    private VBox mainbox;

    @FXML
    private VBox serverbox;
    @FXML
    private VBox clientbox;

    @FXML
    private TextField hostNameField;

    @FXML
    private TextField clientNameField;

    @FXML
    private TextField serverIP;

    private Socket socket;



    @FXML
    private void startServer(ActionEvent event) throws Exception{
        GameData.getInstance().setServer( true );
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/game.fxml"));
        Parent signup = loader.load();
        GameController controller = loader.getController();
        //use one of components on your scene to get a reference to your scene object.

        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        Scene signupScene = new Scene (signup, 550, 715);
        stage.setScene(signupScene);
        stage.setOnHidden(e -> {controller.shutdown();
                Platform.exit();});
        stage.show();
        stage.setResizable(false);
    }

    @FXML
    private void startClient(ActionEvent event) throws Exception{
        GameData.getInstance().setServer( false );
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/game.fxml"));
        Parent signup = loader.load();

        //use one of components on your scene to get a reference to your scene object.

        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        Scene signupScene = new Scene (signup,  550, 725);
        stage.setScene(signupScene);
        stage.show();
        stage.setResizable(false);
    }

    @FXML
    public void server(){
        mainbox.setVisible( false );
        serverbox.setVisible( true );
    }

    @FXML
    public void host(ActionEvent event) throws Exception {
        if(hostNameField.getText().trim().length() >= 1){
            GameData.getInstance().setName( hostNameField.getText().trim() );
            GameData.getInstance().setGamestate( 0 );
            startServer( event );
        }
    }

    @FXML
    public void join(ActionEvent event){
        mainbox.setVisible( false );
        clientbox.setVisible( true );

    }

    @FXML
    private void connect(ActionEvent event)throws Exception{
        if(clientNameField.getText().trim().length() >= 1) {
            if (serverIP.getText().trim().length() >= 1)
                GameData.getInstance().setName( clientNameField.getText().trim() );
            GameData.getInstance().setGamestate( 0 );
            startClient( event );
        }
    }

}

//    @FXML



