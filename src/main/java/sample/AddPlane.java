package sample;

import javafx.concurrent.Task;

import java.util.concurrent.Callable;

public class AddPlane implements Callable<Plane> {
    private int[] head;
    private int[] tail;



    @Override
    public Plane call() throws Exception {
        System.out.println("enter head of plane");
        //System.out.println(GameData.getInstance().getClickposition()[0]);
        GameData.getInstance().setClickposition(null);

            int[] head;
            int[] tail;
            while (true) {
                if (GameData.getInstance().getClickposition() != null){
                    head = GameData.getInstance().getClickposition();
                    System.out.println("enter tail of the plane");
                    GameData.getInstance().setClickposition(null);
                    while (true) {
                        if (GameData.getInstance().getClickposition() != null){
                            tail = GameData.getInstance().getClickposition();
                            if (tail[0] == head[0]+3 || tail[0] == head[0]-3 || tail[1] == head[1]+3 || tail[1] == head[1]-3){
                                Plane plane = new Plane(makePlane( head,tail ), head);
                                int matrix[][] = plane.getPlane();
                                for (int j = 0; j < 10; j++) {
                                    for (int i = 0; i < 10; i++) {
                                        System.out.print(matrix[i][j] + " ");
                                        if (i == 9) {
                                            System.out.println("\n");
                                        }
                                    }
                                }
                                return plane;

                            }
                        }
                    }
                }
            }
    }

    public int[][] makePlane(int[] head, int[] tail){
        int[][] matrix = new int[10][10];
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {

                if (head[0]==tail[0]) {
                    if (head[1] < tail[1]) {
                        if (i==head[0] && j >= head[1] && j <= tail[1]){
                            matrix[i][j] = 1;
                            if (j == head[1]) {
                                matrix[i - 1][j - 1] = 1;
                                matrix[i + 1][j - 1] = 1;
                                matrix[i - 1][j - 3] = 1;
                                matrix[i + 1][j - 3] = 1;
                            }
                        }
                    } else if (head[1] > tail[1]){
                        if (i==head[0] && j <= head[1] && j >= tail[1]) {
                            matrix[i][j] = 1;
                            if (j == head[1]) {
                                matrix[i - 1][j + 1] = 1;
                                matrix[i + 1][j + 1] = 1;
                                matrix[i - 1][j + 3] = 1;
                                matrix[i + 1][j + 3] = 1;
                            }
                        }
                    }
                } else if (head[1] == tail[1]){
                    if (head[0] < tail[0]) {
                        if (j==head[1] && i >= head[0] && i <= tail[0]){
                            matrix[i][j] = 1;
                            if (i == head[0]) {
                                matrix[i + 1][j - 1] = 1;
                                matrix[i + 1][j + 1] = 1;
                                matrix[i + 3][j - 1] = 1;
                                matrix[i + 3][j + 1] = 1;
                            }
                        }
                    } else if (head[0] > tail[0]){
                        if (i==head[0] && j >= head[1] && j <= tail[1]) {
                            matrix[i][j] = 1;
                            if (j == head[1]) {
                                matrix[i - 1][j - 1] = 1;
                                matrix[i - 1][j + 1] = 1;
                                matrix[i - 3][j - 1] = 1;
                                matrix[i - 3][j + 1] = 1;
                            }
                        }
                    }
                }


            }
        }
        return matrix;
    }



}
