package sample;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class GameController implements Initializable {
    @FXML
    private Button sendHello;

    @FXML
    private Button addPlane;

    @FXML
    private Button deletePlane;

    @FXML
    private Button sendHit;

    @FXML
    private Button ready;

    @FXML
    private GridPane gridPane2;

    @FXML
    private GridPane gridPane1;

    @FXML
    private Label player1Label;

    @FXML
    private Label player2Label;

    @FXML
    private Label player1Score;

    @FXML
    private Label player2Score;

    @FXML
    private TextArea textArea;

    @FXML
    private TextField textField;

    private NetworkConnection connection;
    private int[] gridPane2ClickPosition;
    private int[] gridPane1ClickPosition = null;
    private boolean addingPlane = false;
    private int[] head = null;
    private int[] tail = null;
    private int[][] opponentBoard = new int[10][10];
    private int[][] myBoard = new int[10][10];
    private boolean turn = false;
    private int hitcount = 0;
    ArrayList<Plane> planes = new ArrayList<>();
    private int player1Scorex = 0;
    private int player2Scorex = 0;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        resetOpponentBoard();
        textArea.appendText("Welcome to my Planes game, add 3 planes and press ready, when both players are ready you can start fighting, select the square you want to hit and press Hit\n");
        textArea.setWrapText(true);
        textArea.setEditable( false );
        GameData.getInstance().setGamestate( 0 );
        boolean isServer = GameData.getInstance().isServer();

        this.connection = isServer ? createServer() : createClient();
        try {
            this.connection.startConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (isServer) {
            turn = true;
        } else {
            Platform.runLater( new Runnable() {
                @Override
                public void run() {
                    while (!GameData.getInstance().isClientConnected()){
                        continue;
                    }
                    try {
//                        Thread.sleep( 3500 );
                        connection.send( new Hit(GameData.getInstance().getName()));
//                    } catch (InterruptedException ie){

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } );

        }

        gridPane2.setStyle( "-fx-background-color: F8ECC2" );
        gridPane1.setStyle( "-fx-background-color: F8ECC2" );
        System.out.println( gridPane2ClickPosition );


        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                Label label = new Label();
                label.setStyle( "-fx-background-color: grey" );
                label.setAlignment( Pos.CENTER );
                label.setOnMousePressed( event -> {

                    Platform.runLater( new Runnable() {
                        @Override
                        public void run() {

                            Label label = (Label) event.getSource();
                            System.out.println( gridPane2.getRowIndex( label ) + ", " + gridPane2.getColumnIndex( label ) );
                            //gridPane2ClickPosition[0] = gridPane2.getRowIndex(label);
                            //gridPane2ClickPosition[1] = gridPane2.getColumnIndex(label);
                            int[] position = new int[2];
                            position[0] = gridPane2.getRowIndex( label );
                            position[1] = gridPane2.getColumnIndex( label );
                            GameData.getInstance().setClickposition( position );
                            gridPane2ClickPosition = position;
//====================================================================================================================================
                            if (GameData.getInstance().getGamestate() == 1) {
                                if (GameData.getInstance().getAddOrDelete() == 0) {
                                    if (planes.size() < 3) {
                                        if (head == null) {
                                            System.out.println( "add head" );
                                            head = position;
                                            if (!getValidTailPositions( head ).isEmpty()) {
                                                drawvalidTailPositions( getValidTailPositions( head ) );
                                                for (int[] positions : getValidTailPositions( head )) {
                                                    for (int i = 0; i < positions.length; i++) {
                                                        System.out.print( positions[i] + ", " );

                                                    }
                                                }
                                            } else {
                                                head = null;
                                            }

//                                    System.out.println(getValidTailPositions( head ).toString());
                                        } else if (tail == null) {
                                            System.out.println( "add tail" );

                                            tail = position;
                                            boolean hasValidPosition = false;
                                            for (int[] validPosition : getValidTailPositions( head )) {
                                                if (Arrays.equals( tail, validPosition )) {
                                                    hasValidPosition = true;
                                                }
                                            }
                                            if (hasValidPosition) {
                                                addingPlane = false;
                                                int[][] matrix = makePlane( head, tail );
                                                tail = null;
//                                            GameData.getInstance().setGamestate( 0 );
                                                Plane plane = new Plane( matrix, head );
                                                planes.add( plane );
                                                drawPlanes( planes );
                                                GameData.getInstance().setAddOrDelete( 3 );
                                                for (int i = 0; i < matrix.length; i++) {
                                                    for (int j = 0; j < matrix[i].length; j++) {
                                                        System.out.print( matrix[i][j] + " " );
                                                    }
                                                    System.out.println();
                                                }
                                            } else {
                                                System.out.println( "tail null" );
                                                tail = null;
                                            }


                                        }
                                    } else {
                                        GameData.getInstance().setAddOrDelete( 3 );
                                    }
                                }
//====================================================================================================================================
                                if (GameData.getInstance().getAddOrDelete() == 1) {
                                    if (getPlaneByGridPosition( position ) != null) {
                                        planes.remove( getPlaneByGridPosition( position ) );
                                        drawPlanes( planes );
                                        GameData.getInstance().setAddOrDelete( 3 );
                                    }
                                }
                            }

                        }
                    } );
                } );

                label.setMaxSize( Double.MAX_VALUE, Double.MAX_VALUE );
                gridPane2.add( label, i, j );

            }
        }
        //Label cacat = (Label) getNodeByRowColumnIndex( 1,1,gridPane2 );
        //cacat.setText( "cacat" );
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                Label label = new Label();
                label.setStyle( "-fx-background-color: grey" );
                label.setAlignment( Pos.CENTER );
                label.setOnMousePressed( event -> {
                    Platform.runLater( new Runnable() {
                        @Override
                        public void run() {
                            drawOpponentBoard();
                            Label label = (Label) event.getSource();
                            System.out.println( gridPane1.getRowIndex( label ) + ", " + gridPane1.getColumnIndex( label ) );
                            //gridPane2ClickPosition[0] = gridPane2.getRowIndex(label);
                            //gridPane2ClickPosition[1] = gridPane2.getColumnIndex(label);
                            int[] position = new int[2];
                            position[0] = gridPane1.getRowIndex( label );
                            position[1] = gridPane1.getColumnIndex( label );
                            GameData.getInstance().setClickposition( position );
                            gridPane1ClickPosition = position;
                            if (GameData.getInstance().getGamestate() == 2) {
                                label.setStyle( "-fx-background-color: green");
                            }
                        }

                    } );

                } );
                label.setMaxSize( Double.MAX_VALUE, Double.MAX_VALUE );
                gridPane1.add( label, i, j );
            }
        }
    }

    private Server createServer() {
        GameData.getInstance().setGamestate( 0 );
        player2Label.setText( GameData.getInstance().getName() );
        player1Label.setText( "Not connected" );
        displayScore();

        return new Server( 5555, data -> {
            Platform.runLater( () -> {


                System.out.println( data.toString() );
                System.out.println( data );
                GameData.getInstance().setConn( this.connection );
                if (data instanceof Hit) {
                    Hit hit = (Hit) data;
                    handleHit( hit );
                }
                if (data instanceof String){
                    textArea.appendText(data.toString() + "\n");
                }

            } );
        } );
    }

    private Client createClient() {
        GameData.getInstance().setGamestate( 1);
        player2Label.setText( GameData.getInstance().getName() );
        displayScore();



        return new Client( "90.95.52.86", 5555, data -> {
            Platform.runLater( () -> {
                if (data instanceof Hit) {
                    Hit hit = (Hit) data;
                    handleHit( hit );
                }
                if (data instanceof String){
                    textArea.appendText(data.toString() + "\n");
                }


//            System.out.println( data.toString() );
//            if (data instanceof int[][]) {
//                int[][] matrix = (int[][]) data;
//                System.out.println( matrix[1][1] );
//            }
//
//            if (data instanceof int[]) {
//                int[] aa = (int[]) data;
//                System.out.println( aa[0] );
//            }
//            GameData.getInstance().setConn( this.connection );
            } );
        } );

    }


    @FXML
    private void sendHello() {
        try {
            int[][] scores = {{20, 18, 22, 20, 16},
                    {18, 20, 18, 21, 20},
                    {16, 18, 16, 20, 24},
                    {25, 24, 22, 24, 25}
            };
            connection.send( "hello" );
            connection.send( scores );
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public int[][] makePlane(int[] head, int[] tail) {
        int[][] matrix = new int[10][10];
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {

                if (head[0] == tail[0]) {
                    if (head[1] < tail[1]) {
                        if (i == head[0] && j >= head[1] && j <= tail[1]) {
                            matrix[i][j] = 1;
                            if (j == head[1]) {
                                matrix[i - 1][j + 1] = 1;
                                matrix[i + 1][j + 1] = 1;
                                matrix[i - 1][j + 3] = 1;
                                matrix[i + 1][j + 3] = 1;
                            }
                        }
                    } else if (head[1] > tail[1]) {
                        if (i == head[0] && j <= head[1] && j >= tail[1]) {
                            matrix[i][j] = 1;
                            if (j == head[1]) {
                                matrix[i - 1][j - 1] = 1;
                                matrix[i + 1][j - 1] = 1;
                                matrix[i - 1][j - 3] = 1;
                                matrix[i + 1][j - 3] = 1;
                            }
                        }
                    }
                } else if (head[1] == tail[1]) {
                    if (head[0] < tail[0]) {
                        if (j == head[1] && i >= head[0] && i <= tail[0]) {
                            matrix[i][j] = 1;
                            if (i == head[0]) {
                                matrix[i + 1][j - 1] = 1;
                                matrix[i + 1][j + 1] = 1;
                                matrix[i + 3][j - 1] = 1;
                                matrix[i + 3][j + 1] = 1;
                            }
                        }
                    } else if (head[0] > tail[0]) {
                        if (j == head[1] && i <= head[0] && i >= tail[0]) {
                            matrix[i][j] = 1;
                            if (i == head[0]) {
                                matrix[i - 1][j - 1] = 1;
                                matrix[i - 1][j + 1] = 1;
                                matrix[i - 3][j - 1] = 1;
                                matrix[i - 3][j + 1] = 1;
                            }
                        }
                    }
                }


            }
        }
        return matrix;
    }


    public Node getNodeByRowColumnIndex(final int row, final int column, GridPane gridPane2) {
        Node result = null;
        ObservableList<Node> childrens = gridPane2.getChildren();

        for (Node node : childrens) {
            if (gridPane2.getRowIndex( node ) == row && gridPane2.getColumnIndex( node ) == column) {
                result = node;
                break;
            }
        }

        return result;
    }

    @FXML
    public void addPlane2() {
        head = null;
        tail = null;
        GameData.getInstance().setAddOrDelete( 0 );
    }

    @FXML
    public void deletePlane() {
        drawPlanes( planes );
        GameData.getInstance().setAddOrDelete( 1 );
    }

    @FXML
    public void ready() {
        if (GameData.getInstance().getOpponent()!=null) {
            if (GameData.getInstance().getGamestate() == 1 && planes.size() == 3) {
                GameData.getInstance().setReadyState( GameData.getInstance().getReadyState() + 1 );
                String message = new String( GameData.getInstance().getName() + " is ready!");
                textArea.appendText(message + "\n");
                try {
                    connection.send( new Hit( "ready" ) );
                    connection.send( message);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                textArea.appendText( "Add more planes\n" );
            }
        } else {
            textArea.appendText( "Opponent not connected\n" );
        }
        if (GameData.getInstance().getReadyState() == 2) {
            GameData.getInstance().setGamestate( 2 );

        }

    }

    public ArrayList<int[]> getValidTailPositions(int[] head) {
        int[] corner1 = {0, 0};
        int[] corner2 = {0, 9};
        int[] corner3 = {9, 0};
        int[] corner4 = {9, 9};
        int row;
        int column;
        int[] position = new int[2];
        ArrayList<int[]> positions = new ArrayList<>();
        if (!(Arrays.equals( head, corner1 ) || Arrays.equals( head, corner2 ) || Arrays.equals( head, corner3 ) || Arrays.equals( head, corner4 ))) {

            row = head[0];
            column = head[1] + 3;
            if (!(row == 0 || row == 9)) {
                if ((row <= 9) && (column <= 9) && (row >= 0) && (column >= 0)) {
                    position[0] = row;
                    position[1] = column;
                    Plane plane = new Plane( makePlane( head, position ), head );
                    if (!checkPlanesOverlap( planes, plane )) {
                        positions.add( position );
                    }
                    position = new int[2];

                }
            }

            row = head[0];
            column = head[1] - 3;
            if (!(row == 0 || row == 9)) {
                if ((row <= 9) && (column <= 9) && (row >= 0) && (column >= 0)) {
                    position[0] = row;
                    position[1] = column;
                    Plane plane = new Plane( makePlane( head, position ), head );
                    if (!checkPlanesOverlap( planes, plane )) {
                        positions.add( position );
                    }
                    position = new int[2];
                }
            }

            row = head[0] + 3;
            column = head[1];
            if (!(column == 0 || column == 9)) {
                if ((row <= 9) && (column <= 9) && (row >= 0) && (column >= 0)) {
                    position[0] = row;
                    position[1] = column;
                    Plane plane = new Plane( makePlane( head, position ), head );
                    if (!checkPlanesOverlap( planes, plane )) {
                        positions.add( position );
                    }
                    position = new int[2];
                }
            }

            row = head[0] - 3;
            column = head[1];
            if (!(column == 0 || column == 9)) {
                if ((row <= 9) && (column <= 9) && (row >= 0) && (column >= 0)) {
                    position[0] = row;
                    position[1] = column;
                    Plane plane = new Plane( makePlane( head, position ), head );
                    if (!checkPlanesOverlap( planes, plane )) {
                        positions.add( position );
                    }
                    position = new int[2];
                }
            }
        }


        return positions;
    }

    public boolean checkPlanesOverlap(ArrayList<Plane> planes, Plane planeToCheck) {
        if (!planes.isEmpty()) {
            for (Plane plane : planes) {
                int[][] planeMatrix = plane.getPlane();
                int[][] newPlaneMatrix = planeToCheck.getPlane();
                for (int i = 0; i < 10; i++) {
                    for (int j = 0; j < 10; j++) {
                        if (planeMatrix[i][j] == 1) {
                            if (planeMatrix[i][j] == newPlaneMatrix[i][j]) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }


    public void drawvalidTailPositions(ArrayList<int[]> positions) {
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                for (int[] position : positions) {
                    if (i == position[0] && j == position[1]) {
                        Label label = (Label) getNodeByRowColumnIndex( i, j, gridPane2 );
                        label.setStyle( "-fx-background-color: green" );
                    }
                }
            }

        }

    }

    public void drawPlanes(ArrayList<Plane> planes) {
        int[][] matrix = createFullPlaneMatrix( planes );
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                Label label = (Label) getNodeByRowColumnIndex( i, j, gridPane2 );
                if (!planes.isEmpty()) {
                    if (matrix[i][j] == 1) {
                        label.setStyle( "-fx-background-color: black" );
                    } else {
                        label.setStyle( "-fx-background-color: grey" );
                    }
                } else {
                    label.setStyle( "-fx-background-color: grey" );
                }
            }
        }
    }

    public int[][] createFullPlaneMatrix(ArrayList<Plane> planes) {
        int[][] matrix = new int[10][10];
        if (!planes.isEmpty()) {
            for (int i = 0; i < 10; i++) {
                for (int j = 0; j < 10; j++) {
                    for (Plane plane : planes) {
                        if (plane.getPlane()[i][j] == 1) {
                            matrix[i][j] = 1;
                        }
                    }
                }
            }
            return matrix;

        }
        return null;
    }

    public Plane getPlaneByGridPosition(int[] position) {
        System.out.println( "running" );
        if (!planes.isEmpty()) {
            int[][] fullMatrix = createFullPlaneMatrix( planes );
            for (Plane plane : planes) {
                int[][] matrix = plane.getPlane();
                for (int i = 0; i < 10; i++) {
                    for (int j = 0; j < 10; j++) {

                        if (matrix[position[0]][position[1]] == 1 && matrix[i][j] == fullMatrix[i][j]) {
                            System.out.println( "running" );
                            return plane;

                        }
                    }
                }
            }
            return null;
        }
        return null;
    }

    public void drawOpponentBoard() {
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                Label label = (Label) getNodeByRowColumnIndex( i, j, gridPane1 );
                if (opponentBoard[i][j] == 1) {

                    label.setText( "\u274C" );
                    label.setStyle( "-fx-background-color: red" );
                } else if (opponentBoard[i][j] == 2) {
                    label.setText( "\u26AB" );
                    label.setStyle( "-fx-background-color: grey" );
                } else {
                    label.setStyle( "-fx-background-color: grey" );
                    label.setText( "" );
                }
            }
        }
        updateHitcount( opponentBoard );
        if (hitcount == 24) {
            sendWin();
        }
    }

    public void updateOppinentBoard(int[] position, int value) {
        opponentBoard[position[0]][position[1]] = value;
        drawOpponentBoard();
    }

    public void headshot(int[][] matrix) {
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (matrix[i][j] == 1) {
                    int[] position = {i, j};
                    updateOppinentBoard( position, 1 );
                }

            }

        }

    }

    public void hitResponse(int[] position) {
        if (getPlaneByGridPosition( position ) != null) {
            Hit hit;
            Plane plane = getPlaneByGridPosition( position );
            int[][] matrix = plane.getPlane();
            int[] head = plane.getHead();
            if (Arrays.equals( position, head )) {
                hit = new Hit( position, plane.getPlane(), "headshot" );
                for (int i = 0; i < 10; i++) {
                    for (int j = 0; j < 10; j++) {
                        if (matrix[i][j] == 1) {
                            myBoard[i][j] = 1;
                        }
                    }
                }
                drawHits();
            } else {
                hit = new Hit( position, "bodyshot" );
                myBoard[position[0]][position[1]] = 1;
                drawHits();
            }

            try {
                connection.send( hit );
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Hit hit = new Hit( position, "miss" );
            myBoard[position[0]][position[1]] = 2;
            drawHits();
            try {
                connection.send( hit );
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    private void sendHit() {
        if (turn) {
            if (!(gridPane1ClickPosition == null)) {
                turn ^= true;
                    Hit hit = new Hit( gridPane1ClickPosition, "hit" );
                    try {

                        connection.send( hit );
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }
        } else {
            textArea.appendText("It's not your turn to hit\n");
        }
    }

    private void handleHit(Hit hit) {
        String message = hit.getMessage();
        switch (message) {
            case "ready":
                GameData.getInstance().setReadyState( GameData.getInstance().getReadyState() + 1 );
                if (GameData.getInstance().getReadyState() == 2) {
                    GameData.getInstance().setGamestate( 2 );
                    String messagex;
                    if (turn) {
                        messagex = new String( "It's " + GameData.getInstance().getName() + " turn to shoot!" );
                    } else {
                        messagex = new String( "It's " + GameData.getInstance().getOpponent() + " turn to shoot!" );
                    }
                    textArea.appendText(messagex + "\n");
                    try {
                        connection.send( messagex);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                break;

            case "bodyshot":
                updateOppinentBoard( hit.getHit(), 1 );
                break;

            case "miss":
                updateOppinentBoard( hit.getHit(), 2 );
                break;

            case "headshot":
                int[][] plane = hit.getPlane();
                headshot( plane );
                break;

            case "hit":
                String messagex;
                    messagex = new String( "It's " + GameData.getInstance().getName() + " turn to shoot!" );
                textArea.appendText(messagex + "\n");
                try {
                    connection.send( messagex);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                hitResponse( hit.getHit() );
                System.out.println( hit.getHit()[0] );
                System.out.println( hit.getHit()[1] );
                turn ^= true;
                break;

            case "win":
                handleWin();
                break;

            case "ok":
                break;

            default:
                GameData.getInstance().setGamestate( 1);
                if (GameData.getInstance().getOpponent()==null) {
                    GameData.getInstance().setOpponent( hit.getMessage() );
                    player1Label.setText( hit.getMessage() );

                    Hit nameHit = new Hit( GameData.getInstance().getName() );
                    try {
                        connection.send( nameHit );
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        connection.send( new Hit("ok"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;


        }
    }

    private void updateHitcount(int[][] matrix) {
        hitcount = 0;
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (matrix[i][j] == 1) {
                    hitcount++;
                }
            }
        }
    }

    private void sendWin() {
        Hit hit = new Hit( "win" );
        try {
            connection.send( hit );
        } catch (Exception e) {
            e.printStackTrace();
        }

        gridPane1ClickPosition = null;
        gridPane2ClickPosition = null;
        turn = true;
        GameData.getInstance().setGamestate( 1 );
        GameData.getInstance().setReadyState( 0 );
        planes.removeAll( planes );
        redrawMyBoard();
        resetOpponentBoard();
        drawOpponentBoard();
        player2Scorex +=1;
        displayScore();


    }

    private void handleWin() {
        gridPane1ClickPosition = null;
        gridPane2ClickPosition = null;
        turn = false;
        GameData.getInstance().setGamestate( 1 );
        GameData.getInstance().setReadyState( 0 );
        planes.removeAll( planes );
        redrawMyBoard();
        resetOpponentBoard();
        drawOpponentBoard();
        player1Scorex +=1;
        displayScore();
    }

    private void resetOpponentBoard() {
        hitcount = 0;
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                opponentBoard[i][j] = 0;
            }
        }

    }

    private void redrawMyBoard() {
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                Label label = (Label) getNodeByRowColumnIndex( i, j, gridPane2 );

                label.setStyle( "-fx-background-color: grey; -fx-text-fill : black" );
                label.setText( "" );


            }
        }
    }

    private void drawHits() {
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                Label label = (Label) getNodeByRowColumnIndex( i, j, gridPane2 );
                if (myBoard[i][j] == 1) {

                    label.setText( "\u274C" );
                    label.setStyle( "-fx-background-color: black; -fx-text-fill : red" );
                } else if (myBoard[i][j] == 2) {
                    label.setText( "\u26AB" );
                }
            }
        }


    }

    public void displayScore(){
        player1Score.setText( "Score: " + player1Scorex );
        player2Score.setText( "Score: " + player2Scorex );
    }

    public void shutdown() {
        try {
            connection.closeConnection();
        }catch (Exception e){
            e.printStackTrace();
        }

    }
    @FXML
    private void sendMessage(){
        if (GameData.getInstance().getOpponent() != null) {
            String message = new String( GameData.getInstance().getName() + ": " + textField.getText() );
            textArea.appendText(message + "\n");
            textField.clear();
            try {
                connection.send( message );
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}


