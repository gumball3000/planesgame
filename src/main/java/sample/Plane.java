package sample;

public class Plane {
    private int[][] plane;
    private int[] head;

    public Plane(int[][] plane, int[] head) {
        this.plane = plane;
        this.head = head;
    }

    public int[][] getPlane() {
        return plane;
    }

    public int[] getHead() {
        return head;
    }
}
