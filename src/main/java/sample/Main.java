package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.commons.lang3.RandomStringUtils;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader( getClass().getResource( "/fxml/sample.fxml" ) );
        Parent root = loader.load();
        primaryStage.setTitle("Planes");
        primaryStage.setScene(new Scene(root, 311, 311));
        primaryStage.show();

//        URL url = new URL("http://checkip.amazonaws.com/");
//        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
//        System.out.println(br.readLine());
//        Alien alien = new Alien();
//        alien.setAid( 106 );
//        alien.setAname( "gumbix");
//        alien.setColor( "orange" );
//
//        Configuration cfg = new Configuration().configure().addAnnotatedClass( Alien.class );
//
//        ServiceRegistry reg = new ServiceRegistryBuilder(  ).applySettings( cfg.getProperties() ).buildServiceRegistry();
//
//        SessionFactory sf = cfg.buildSessionFactory(reg);
//
//        Session session = sf.openSession();
//
//        Transaction transaction = session.beginTransaction();
//
//        session.save( alien );
//        alien = (Alien) session.get( Alien.class,103 );
//        transaction.commit();
//        System.out.println(alien);
        System.out.println("\u274C");
        System.out.println( RandomStringUtils.randomAlphanumeric(10));
    }


    public static void main(String[] args) {
        launch(args);
    }


}
