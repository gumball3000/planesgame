package sample;

import java.util.List;

public class GameData {
    private String name;
    private String opponent;
    private String IP;
    private boolean isServer;
    private NetworkConnection conn;
    private List<Plane> planes;
    private int[] clickposition = null;
    private int gamestate;
    private int addOrDelete;
    private int readyState = 0;
    private static GameData instance = new GameData();
    private static boolean clientConnected = false;


    public GameData() {
    }

    public GameData(String name, String opponent, boolean isServer) {
        this.name = name;
        this.opponent = opponent;
        this.isServer = isServer;
    }

    public static GameData getInstance() {
        return instance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOpponent() {
        return opponent;
    }

    public void setOpponent(String opponent) {
        this.opponent = opponent;
    }

    public boolean isServer() {
        return isServer;
    }

    public void setServer(boolean server) {
        isServer = server;
    }

    public void closeConnection() throws Exception{
        conn.closeConnection();
    }

    public void setConn(NetworkConnection conn) {
        this.conn = conn;
    }

    public int[] getClickposition() {
        return clickposition;
    }

    public void setClickposition(int[] clickposition) {
        this.clickposition = clickposition;
    }

    public int getGamestate() {
        return gamestate;
    }

    public void setGamestate(int gamestate) {
        this.gamestate = gamestate;
    }

    public int getAddOrDelete() {
        return addOrDelete;
    }

    public void setAddOrDelete(int addOrDelete) {
        this.addOrDelete = addOrDelete;
    }

    public int getReadyState() {
        return readyState;
    }

    public void setReadyState(int readyState) {
        this.readyState = readyState;
    }

    public String getIP() {
        return IP;
    }

    public void setIP(String IP) {
        this.IP = IP;
    }

    public static boolean isClientConnected() {
        return clientConnected;
    }

    public static void setClientConnected(boolean clientConnected) {
        GameData.clientConnected = clientConnected;
    }
}
